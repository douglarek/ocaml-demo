open Core.Std

module Log_entry = struct
  type t = {
      important: bool;
      message: string;
    }
end

module Heartbeat = struct
  type t = {
      status_message: string;
    }
end

module Logon = struct
  type t = {
      user: string;
      credentials: string;
    }
end

type details =
  | Logon of Logon.t
  | Heartbeat of Heartbeat.t
  | Log_entry of Log_entry.t

module Common = struct
  type t = {
      session_id: string;
      time: Time.t;
    }
end
